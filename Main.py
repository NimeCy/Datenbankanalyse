import datetime

import pandas as pd
from sqlalchemy import create_engine
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np
import seaborn as sns




def LoadData():
    # SQLAlchemy-Engine erstellen
    engine = create_engine('postgresql://sven:Uine-Cy5917!@134.106.62.235:5432/postgres')
    # Daten aus der Datenbank abrufen

    query = """                   SELECT 
                        'SoSe_24' AS year,
                        CONCAT('Gruppe', group_id) AS team,
                        station_name,
                        sensor_name,
                        timestamp,
                        value
                    FROM 
                        human_readable_merge_view;"""

    df = pd.read_sql(query, con=engine)
    df['timestamp'] = pd.to_datetime(df['timestamp'])
    return df

# Funktion zur Zählung der Einträge pro Jahr und Team
def count_entries_per_year_and_team(df):
    df =  df.groupby(['year', 'team']).size().reset_index(name='Anzahl der Datensätze')
    return df.sort_values(by='Anzahl der Datensätze', ascending=False)

def filter_by_range(df):
    # SQLAlchemy-Engine erstellen
    engine = create_engine('postgresql://sven:Uine-Cy5917!@134.106.62.235:5432/postgres')

    sensor_query = """
    SELECT 
        name AS sensor_name, 
        measurement_range_min, 
        measurement_range_max 
    FROM 
        sensor_type;
    """
    sensor_ranges = pd.read_sql_query(sensor_query, engine)

    # Min- und Max-Werte der Sensoren zu den Daten hinzufügen
    df = df.merge(sensor_ranges, on='sensor_name', how='left')

    # Gesamtanzahl der Werte pro Jahr und Team zählen
    total_count = df.groupby(['year', 'team']).size().reset_index(name='total_count')

    # Werte außerhalb des Messbereichs filtern
    out_of_range = df[(df['value'] < df['measurement_range_min']) | (df['value'] > df['measurement_range_max'])]

    # Anzahl der Werte außerhalb des Messbereichs pro Jahr, Team und Sensor zählen
    out_of_range_count = out_of_range.groupby(['year', 'team', 'sensor_name']).size().reset_index(
        name='out_of_range_count')

    # Pivot-Tabelle erstellen
    pivot_table = out_of_range_count.pivot_table(index=['year', 'team'], columns='sensor_name',
                                                 values='out_of_range_count', fill_value=0)

    # Pivot-Tabelle zurück in DataFrame umwandeln und Spaltennamen zurücksetzen
    pivot_table = pivot_table.reset_index()

    # Gesamtanzahl der Werte pro Jahr und Team zu Pivot-Tabelle hinzufügen
    pivot_table = pivot_table.merge(total_count, on=['year', 'team'], how='left')

    # Prozentualen Anteil der Werte außerhalb des Messbereichs berechnen
    for sensor in sensor_ranges['sensor_name']:
        if sensor in pivot_table.columns:
            pivot_table[sensor] = (pivot_table[sensor] / pivot_table['total_count']) * 100

    # 'total_count'-Spalte entfernen, da sie nicht mehr benötigt wird
    pivot_table = pivot_table.drop(columns=['total_count'])

    # print(pivot_table)

    # Werte innerhalb des Messbereichs filtern
    filtered_df = df[(df['value'] >= df['measurement_range_min']) & (df['value'] <= df['measurement_range_max'])]

    # Unnötige Spalten entfernen
    filtered_df = filtered_df.drop(columns=['measurement_range_min', 'measurement_range_max'])

    return filtered_df


# Funktion zur Erstellung des Scatter-Plots
def show_scatter_plots_for_each_sensor(df):
    # Konvertiere den Timestamp in ein datetime-Objekt
    df['timestamp'] = pd.to_datetime(df['timestamp'])

    # Erstelle eine Liste der einzigartigen Sensornamen
    sensors = df['sensor_name'].unique()

    for sensor in sensors:
        sensor_data = df[df['sensor_name'] == sensor]

        # Plot Dimensionen
        plt.figure(figsize=(20, 6))

        # Scattern
        sns.scatterplot(data=sensor_data, x='timestamp', y='value',color='blue')

        plt.title(f'Scatterplot für {sensor}')
        plt.xlabel('Zeitstempel')
        plt.ylabel('Wert')

        # Plot rotieren damit er besser lesbar ist
        plt.xticks(rotation=45)

        #Plot anzeigen
        plt.show()

        # Schließen
        plt.close()


def filter_and_convert_pressure(df, pressure_column='value', sensor_column='sensor_name'):
    # Kopie des DataFrames erstellen, um das Original nicht zu verändern
    df_copy = df.copy()

    # Funktion zur Identifizierung und Umrechnung von Pa in hPa
    def convert_to_hpa(row):
        if row[sensor_column] == 'BMP280_PRESSURE':
            if 0 <= row[pressure_column] < 12000:  # Werte zwischen 20000 und 80000
                return row[pressure_column] * 100  # Umrechnung von  Pa in hPa
        return row[pressure_column]  # Andere Sensoren oder bereits in hPa bleiben unverändert

    # Anwenden der Konvertierungsfunktion auf die relevanten Zeilen
    df_copy[pressure_column] = df_copy.apply(convert_to_hpa, axis=1)

    return df_copy


def plot_scatter_for_sensors(df):
    # Konvertiere den Timestamp in ein datetime-Objekt
    df['timestamp'] = pd.to_datetime(df['timestamp'])

    #Plotgröße fest legen
    plt.figure(figsize=(12, 6))

    # Erstelle einen Scatterplot für jeden Sensor mit unterschiedlichen Farben
    sensors = df['sensor_name'].unique()
    for sensor in sensors:
        sensor_data = df[df['sensor_name'] == sensor]
        plt.scatter(sensor_data['timestamp'], sensor_data['value'], label=sensor, alpha=0.7)

    #Plot einstellen
    plt.title('Scatterplot aller Sensoren')
    plt.xlabel('Zeitstempel')
    plt.ylabel('Wert')
    plt.legend(loc='upper right')
    plt.xticks(rotation=45)
    plt.xlim(df['timestamp'].min(), df['timestamp'].max())
    plt.tight_layout()

    plt.show()


def create_boxplots_for_sensors(df):
    # Eindeutige Sensornamen ermitteln
    sensors = df['sensor_name'].unique()

    # Für jeden Sensor einen Boxplot erstellen
    for sensor in sensors:
        # Daten für den aktuellen Sensor filtern
        sensor_data = df[df['sensor_name'] == sensor]

        # Neue Figur erstellen
        plt.figure(figsize=(10, 6))

        # Boxplot erstellen
        sns.boxplot(x='sensor_name', y='value', data=sensor_data)

        # Titel und Achsenbeschriftungen setzen
        plt.title(f'Boxplot für {sensor}')
        plt.xlabel('Sensor')
        plt.ylabel('Wert')

        # Plot anzeigen
        plt.show()

        # Figur schließen, um Speicher freizugeben
        plt.close()


def remove_indoor_stations(df,threshold=50):
    # Filter den DataFrame, um nur Temperaturwerte zu behalten
    # Filtern Sie den DataFrame, um nur Temperaturwerte zu behalten
    temp_df = df[df['sensor_name'].str.contains('TEMP', case=False, na=False)]

    # Gruppieren nach Station und Berechnung der Statistiken
    temp_stats = temp_df.groupby('station_name')['value'].agg(['min', 'max', 'mean']).reset_index()

    # Berechnen der Schwankung
    temp_stats['fluctuation'] = temp_stats['max'] - temp_stats['min']

    # Berechnen der prozentualen Schwankung
    temp_stats['percent_fluctuation'] = (temp_stats['fluctuation'] / np.abs(temp_stats['mean'])) * 100

    # Identifizieren der Stationen mit den geringsten Schwankungen
    indoor_stations = temp_stats[temp_stats['percent_fluctuation'] < threshold]['station_name']

    # Entfernen der Indoor-Stationen aus dem ursprünglichen DataFrame
    filtered_df = df[~df['station_name'].isin(indoor_stations)]

    return filtered_df


def remove_outliers_by_sensor(df, threshold=3.0):
    df_cleaned = df.copy()
    print("Ursprüngliche Sensoren:", df['sensor_name'].unique())

    # Gruppieren nach Sensor
    grouped = df_cleaned.groupby('sensor_name')

    # Funktion zur Ausreißererkennung für einen einzelnen Sensor
    def remove_outliers_single_sensor(sensor_data):
        Q1 = sensor_data['value'].quantile(0.25)
        Q3 = sensor_data['value'].quantile(0.75)
        IQR = Q3 - Q1
        lower_bound = Q1 - threshold * IQR
        upper_bound = Q3 + threshold * IQR
        return sensor_data[(sensor_data['value'] >= lower_bound) & (sensor_data['value'] <= upper_bound)]

    # Anwenden der Ausreißererkennung auf jeden Sensor separat
    df_cleaned = grouped.apply(remove_outliers_single_sensor).reset_index(drop=True)

    print("Sensoren nach der Bereinigung:", df_cleaned['sensor_name'].unique())

    return df_cleaned


def show_sensor_correlation_matrix(df):
    # Konvertiert den Timestamp in ein datetime-Objekt, falls noch nicht geschehen
    df['timestamp'] = pd.to_datetime(df['timestamp'])

    # Pivot-Tabelle erstellen: Jede Spalte ist ein Sensor, Zeilen sind Zeitpunkte
    # aggfunc='mean' -> Führt ansonsten zu gleichen Indexen
    pivot_df = df.pivot_table(index='timestamp', columns='sensor_name', values='value', aggfunc='mean')

    # Fehlende Werte interpolieren, um sicherzustellen, dass alle Sensoren synchronisiert sind
    pivot_df = pivot_df.interpolate(method='time')

    # Berechnen der Korrelationsmatrix
    corr_matrix = pivot_df.corr(method='pearson')

    # Erstellen der Heatmap
    plt.figure(figsize=(12, 10))
    sns.heatmap(corr_matrix,
                annot=True,
                cmap='coolwarm',
                vmin=-1,
                vmax=1,
                center=0,
                square=True,
                linewidths=.5,
                cbar_kws={"shrink": .8})

    plt.title('Korrelationsmatrix zwischen Sensoren')
    plt.tight_layout()
    plt.show()

    return corr_matrix






# Hauptteil des Skripts
if __name__ == "__main__":
    # Daten für alte Daten abrufen und anzeigen
    df_all = LoadData()



    # Pandas-Optionen setzen, um alle Spalten anzuzeigen
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', 1000)


    #Daten anschauen was alles vorhanden ist, dient vorallem um die schauen wie die Tabellen aufgebaut sind.
    # print("Daten")
    # print(df_all.head(10),"\n\n")

    #Daten der jeweiligen Gruppen aus den jeweiligen Jahren anschauen
    # print("Ungefilterte Anzahl")
    # print(count_entries_per_year_and_team(df_all),"\n\n")


    #Messgrenzen ermitteln und Daten filtern.
    # print("Gefilterte Anzahl mit Prozentualem Fehler")
    df_filtered = filter_by_range(df_all)
    # print(count_entries_per_year_and_team(df_filtered),"\n\n")

    #Druck ist Fehlerhaft wegen mir - Anpassen
    # print("Scatterplots anzeigen")
    df_pressure = filter_and_convert_pressure(df_filtered)
    # show_scatter_plots_for_each_sensor(df_pressure)


    # print("Scatterplots der verschiedenen Daten", "\n\n")
    # plot_scatter_for_sensors(df_pressure)

    # print("Boxplot", "\n\n")
    # create_boxplots_for_sensors(df_pressure)
    # print("\n")
    df_without_indoor = remove_indoor_stations(df_pressure)
    # print(df_pressure['sensor_name'].unique())
    # print(count_entries_per_year_and_team(df_without_indoor),"\n\n")


    # create_boxplots_for_sensors(df_pressure)
    df_whisker = remove_outliers_by_sensor(df_without_indoor)
    # print(count_entries_per_year_and_team(df_whisker),"\n\n")
    # print(df_without_indoor['sensor_name'].unique())
    # show_scatter_plots_for_each_sensor(df_without_indoor)
    # plot_scatter_for_sensors(df_pressure)
    # plot_scatter_for_sensors(df_without_indoor)
    # plot_scatter_for_sensors(df_whisker)


    # show_scatter_plots_for_each_sensor(df_whisker)
    # create_boxplots_for_sensors(df_whisker)

    show_sensor_correlation_matrix(df_whisker)

    # create_boxplots_for_sensors(df_whisker)






